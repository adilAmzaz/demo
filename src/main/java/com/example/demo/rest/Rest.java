package com.example.demo.rest;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;  
@RestController
class Rest {

    @RequestMapping("/")
    String home() {
        return "Hello, World!";
    }

}
